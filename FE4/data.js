let songs = [
{
    name: 'Chivalry is Dead',
    path: 'music/Chivalry Is Dead.mp3',
    artist: 'Trevor Wesley'

},
{
    name: 'Binibini',
    path: 'music/binibini.mp3',
    artist: 'Zach Tabudlo'

},
{
    name: 'Mabagal',
    path: 'music/Mabagal.mp3',
    artist: 'Daniel Padilla & Moira Dela Torre'

},
{
    name: 'Buwan',
    path: 'music/buwan.mp3',
    artist: 'Juan Carlos'

},
{
    name: 'Somebody out There',
    path: 'music/Somebody Out There.mp3',
    artist: 'A Rocket to the Moon'

},

]